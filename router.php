<?php
/**
 * @author Lauris Grosu celice95@gmail.com
 */
class Router
{
    function __construct()
    {
        $this->controller = (isset($_GET['controller'])? $_GET['controller']: "HomeController");
        $this->controller_r = false;
        $this->stop_load = false;
        $this->action = (isset($_GET['action'])? $_GET['action']: "getIndex");
        $this->title = "";
        $this->controllers = array(
            "HomeController" => ['getIndex', 'error', ['Home page', false]],
            "ProductController" => ['getOne', 'addProduct', 'error', ['Product page', false]],
            "AjaxController" => ['addProduct', 'checkSKU', 'deleteProduct','test', ['Ajax page', true]]
        );
        $this->checkController();
    }

    /**
     * Checking if controller exists
     */
    public function checkController()
    {
        if (array_key_exists($this->controller, $this->controllers)) {
            if (in_array($this->action, $this->controllers[$this->controller])) {
                $this->controller_r = true;
                $this->title = $this->controllers[$this->controller][count($this->controllers[$this->controller]) - 1][0];
                $this->stop_load = $this->controllers[$this->controller][count($this->controllers[$this->controller]) - 1][1];
            } else {
                $this->controller_r = false;
            }
        } else {
            $this->controller_r = false;
        }
    }

    /**
     * Loading controller
     */
    function start()
    {
        if ($this->controller_r) {
            if (!$this->stop_load){
                require_once "view/layout.php";
            } else {
                $this->call($this->controller, $this->action);
            }
        } else {
            $this->call('HomeController', 'error');
        }
    }

    /**
     * Calling controller
     * @param $controller
     * @param $action
     */
    function call($controller, $action)
    {
        require_once "controller/" . $controller . ".php";
        switch ($controller) {
            case "HomeController":
                $controller = new homeController("", "Home");
                break;
            case "ProductController":
                $controller = new productController("product", "Product");
                break;
            case "AjaxController":
                $controller = new ajaxController("ajax", "Ajax");
                break;
        }
        $controller->{$action}();
    }

    /**
     * Setting page title
     * @return string
     */
    public function setTitle()
    {
        $title = $this->title;
        return isset($title) && !empty($title) ? $title : "SW Project";
    }
}
