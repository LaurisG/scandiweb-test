<?php
/**
 * @author Lauris Grosu celice95@gmail.com
 */
class Database
{
    protected $_pdo;
    protected $_cfg;
    public $_error;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        global $config;
        $this->_cfg = $config['database'];
    }

    /**
     * Submit query to mysql
     * @param $query
     * @return mixed
     */
    public function query($query)
    {
        $this->connect();
        if (!$result = $this->_con->query($query)) {
            printf("Errormessage: %s\n", $this->_con->error);
        }
        $this->closeConn();

        return $result;
    }

    /**
     * Create connection
     */
    public function connect()
    {
        $dsn = "mysql:host={$this->_cfg['host']};dbname={$this->_cfg['name']};charset={$this->_cfg['char']}";
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        try {
            $this->_pdo = new PDO($dsn, $this->_cfg['user'], $this->_cfg['pass'], $opt);
        } catch (PDOException $e) {
            die('Connection failed: ' . $e->getMessage());
        }
    }

    /**
     * Selecting all products
     * @param $select
     * @param $table
     * @return string
     */
    public function selectAll($select, $table)
    {
        $this->connect();
        try {
            $sth = $this->_pdo->query("SELECT {$select} FROM {$table}");
            return $sth->fetchAll();
        } catch (PDOException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Inserting products
     * @param $param
     * @return string
     */
    public function insert($param)
    {
        $this->connect();
        try {
            $sth = $this->_pdo->prepare("INSERT INTO `products_`(`sku`, `name`, `type`, `price`, `height`, `width`, `size`, `lenght`, `weight`) VALUES (:sku, :name, :type, :price, :height, :width, :size, :lenght, :weight )");
            return $sth->execute($param);
        } catch (PDOException $e) {
            $this->_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Getting count of rows
     * @param $where
     * @param $value
     * @return string
     */
    public function getCountOfRows($where, $value)
    {
        $this->connect();
        try {
            $sth = $this->_pdo->query('SELECT COUNT(*) FROM `products_` WHERE ' . $where . ' = "' . $value . '"');
            return $sth->fetchColumn();
        } catch (PDOException $e) {
            $this->_error = $e->getMessage();
            return false;
        }
    }

    /**
     * Deleting product from database by SKU
     * @param $sku
     * @return string
     */
    public function deleteProduct($sku)
    {
        $this->connect();
        try {
            return $this->_pdo->query('DELETE FROM `products_` WHERE sku = "' . $sku . '"');
        } catch (PDOException $e) {
            $this->_error = $e->getMessage();
            return false;
        }
    }
}
