class Core{
    constructor(){
        self = this;
    }

    show_alert(message, type = 'success') {
        $("body").append("<div id='msg_box' class='" + type + "'>" + message + "</div>");
        setTimeout(function () {
            $("#msg_box").remove();
        }, 2000);
    }

    addProduct(sku, name, price, type, type_values) {
        self.show_alert("Saving product...");
        $.ajax({
            type: 'POST',
            url: "?controller=AjaxController&action=addProduct",
            data: {
                'sku': sku,
                'name': name,
                'price': price,
                'type': type,
                'attr': type_values,
                'action': "addProduct"
            },
            success: function (msg) {
                if (msg === "OK"){
                    self.show_alert("Product added success!");
                    $("#add_form").trigger('reset');
                } else {
                    self.show_alert("Something went wrong.. ", "error");
                }
            },
            error: function () {
                self.show_alert("Something went wrong.. ", "error");
            },
            async: false
        });
    }

    deleteProducts(data) {
        $.ajax({
            type: 'POST',
            url: "?controller=AjaxController&action=deleteProduct",
            data: {
                "sku": data,
                "action": "deleteProduct"
            },
            success: function (msg) {
                if (msg === "OK"){
                    self.show_alert("Products delete success..reloading page");
                    $('input:checkbox').removeAttr('checked');
                    setInterval(function () {
                        location.reload();
                    }, 100);
                } else {
                    self.show_alert("Something went wrong.. ", "error");
                }
            },
            error: function () {
                self.show_alert("Something went wrong.. ", "error");
            }
        });
    }

    checkSKU(sku) {
        let uniq;
        let count;
        $.ajax({
            type: 'POST',
            url: "?controller=AjaxController&action=checkSKU",
            data: {
                "action": "checkSKU",
                "sku": sku
            },
            success: function (msg) {
                count = msg;
                if (count == 0) {
                    uniq = true;
                } else {
                    uniq = false;
                }
            },
            error: function () {
                uniq = false;
                self.show_alert("Something went wrong.. ", "error");
            },
            async: false

        });

        return uniq;
    }
}
