let web = new Core();

$("#save_data").click(function () {
    let form = $("#add_form");
    form.validate();
    if (form.valid()) {
        if (web.checkSKU($("input[name=sku]").val())) {
            $("#form_submit").click();
        } else {
            web.show_alert("SKU must to be unique", "error");
        }
    }
});

$("#select_type").on("change", function (e) {
    let element = '#' + this.value;
    let type = $("#select_type").val();
    let panel = $("#" + type);
    let inputs = panel.find("input");
    $(".type_block").hide();
    $(element).show();
    $('.second-form').find("input").filter('[required]').prop('required', false);
    $(inputs).prop('required', true);
});

$("#add_form").submit(function () {
    let type = $("#select_type").val();
    let panel = $("#" + type);
    let inputs = panel.find("input");
    let type_values = {};
    let sku = $("input[name=sku]").val();
    let name = $("input[name=name]").val();
    let price = $("input[name=price]").val();
    for (i = 0; i < inputs.length; i++) {
        type_values[inputs[i].name] = inputs[i].value;
    }
    web.addProduct(sku, name, price, type, type_values);

    return false;
});

$("#applyButton").click(function () {
    let action = $('#action').find(":selected").val();
    let data = [];
    if (action === "delete") {
        $("input:checkbox:checked").each(function () {
            data.push($(this).val());
        });
        if (data.length !== 0) {
            web.deleteProducts(data);
        }
    }
});
