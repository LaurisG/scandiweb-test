
## Scandiweb Junior test

Basic MVC based page, where you can store products. Simple design, simple functionality.

---

## Compiling

* SASS is used in this project
* Page colors can be configured in _config.scss
---
To compile CSS run compass compile from root directory.

---

## Database config

* PDO is used in this project
---
* Database connection can be configured under config.php in root directory.
* Rename config.php.example to config.php and enter database connection details.
