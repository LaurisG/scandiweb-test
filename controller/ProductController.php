<?php
/**
 * @author Lauris Grosu celice95@gmail.com
 */
class productController extends Abstract_Controller
{
    /**
     * Load add product page
     */
    public function addProduct()
    {
        parent::loadView('addProduct');
    }
}
