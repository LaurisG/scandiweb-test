<?php
/**
 * @author Lauris Grosu celice95@gmail.com
 */
class homeController extends Abstract_Controller
{
    /**
     * Loading index page
     */
    public function getIndex()
    {
        $data = $this->model->getAllProducts();
        parent::loadView("allProducts", $data);
    }
}
