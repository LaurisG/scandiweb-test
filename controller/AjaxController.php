<?php
/**
 * @author Lauris Grosu celice95@gmail.com
 */
class AjaxController extends Abstract_Controller
{
    /**
     * Ajax request to add product
     */
    public function addProduct()
    {
        echo $this->model->writeProduct();
    }

    /**
     * Ajax request to delete products
     */
    public function deleteProduct()
    {
        echo $this->model->deleteProducts();
    }

    /**
     * Ajax request to check, if sku exists
     */
    public function checkSKU()
    {
        echo $this->model->checkSKU();
    }
}
