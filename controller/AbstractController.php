<?php
/**
 * @author Lauris Grosu celice95@gmail.com
 */
class Abstract_Controller
{
    public $pageName;
    public $model;

    /**
     * Abstract_Controller constructor.
     * @param $model
     * @param $pageName
     */
    public function __construct($model, $pageName)
    {
        $this->model = new Model($model);
        $this->pageName = $pageName;
    }

    /**
     * Load template file
     * @param $file
     * @param null $data
     */
    public function loadView($file, $data = NULL)
    {
        if (file_exists("view/{$file}.php")) {
            require "view/{$file}.php";
        } else {
            echo $this->getError();
        }
    }

    /**
     * Error message
     */
    public function getError()
    {
        return "Error";
    }
}
