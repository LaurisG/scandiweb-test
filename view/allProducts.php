<div class="content">
    <div class="item-list">
        <?php
            if (!empty($data)):
                foreach ($data as $row):  ?>
                    <div class="item">
                        <input type="checkbox" value="<?= $row['sku']; ?>"/>
                        <div class="info">
                            <span><?= $row['sku']; ?></span>
                            <span><?= $row['name']; ?></span>
                            <span><?= $row['price']; ?>$</span>
                            <?php if ($row['type'] == "furniture"): ?>
                               <span>Dimensions: <?= $row['height']; ?>x<?= $row['width']; ?>x<?= $row['lenght']; ?></span>
                            <?php elseif ($row['type'] == "book") : ?>
                                <span>Weight: <?= $row['weight']; ?>kg</span>
                            <?php elseif ($row['type'] == "disc"): ?>
                                <span>Size: <?= $row['size']; ?>mb</span>
                             <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach;
            else:
                echo "No data found, please add data..";
            endif;
        ?>
    </div>
</div>
