<div class="product_content">
    <form action="" method="post" id="add_form">
        <div>
            <span>SKU</span>
            <input name="sku" type="text" required>
        </div>
        <div>
            <span>Name</span>
            <input name="name" type="text" required>
        </div>
        <div>
            <span>Price</span>
            <input name="price" type="number" required>
        </div>
        <div>
            <span>Type switcher</span>
            <select name="type" id="select_type" required>
                <option disabled selected>Chose type</option>
                <option value="disc">DVD-disc</option>
                <option value="book">Book</option>
                <option value="furniture">Furniture</option>
            </select>
        </div>
        <div class="second-form">
            <div class="type_block" id="disc" style="display: none">
                <div>
                    <span>Size:</span>
                    <input name="size" type="text" placeholder="">
                </div>
                <div class="desc">
                    <i>Please enter size of disc in megabytes ( MB )</i>
                </div>
            </div>
            <div class="type_block" id="furniture" style="display: none">
                <div>
                    <span>Height:</span>
                    <input name="height" type="text" placeholder="">
                </div>
                <div>
                    <span>Width:</span>
                    <input name="width" type="text" placeholder="">
                </div>
                <div>
                    <span>Length:</span>
                    <input name="length" type="text" placeholder="">
                </div>
                <div class="desc">
                    <i>Enter dimensions of furniture in cm</i>
                </div>
            </div>
            <div class="type_block" id="book" style="display: none">

                <div>
                    <span>Weight:</span>
                    <input name="weight" type="text" placeholder="">
                </div>
                <div class="desc">
                    <i>Please enter weight of book in killograms (kg)</i>
                </div>
            </div>
        </div>
        <input type="submit" id="form_submit" style="display:none;"/>
    </form>
</div>
