<html>
<head>
    <link href="assets/css/main.css" rel="stylesheet" type="text/css"/>
    <meta charset="UTF-8"/>
    <title><?= $this->setTitle(); ?></title>
</head>
<body>
<div class="header">
    <div class="title">
        <a href="?controller=HomeController&action=getIndex">Home page</a> -
        <a href="?controller=ProductController&action=addProduct">Add product</a>
    </div>
    <?php if ($this->controller == "ProductController"): ?>
           <div class="action">
                <button id="save_data">Save</button>
            </div>
       <?php else: ?>
            <div class="action" id="action">
                <select>
                    <option disabled selected>chose something</option>
                    <option value="delete">Mass delete action</option>
                </select>
                <button id="applyButton">Apply</button>
            </div>
        <?php endif; ?>
</div>
<?php
    $this->call($this->controller, $this->action);
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script type="text/javascript" src="assets/js/core.js"></script>
<script type="text/javascript" src="assets/js/script.js"></script>
</body>
</html>
