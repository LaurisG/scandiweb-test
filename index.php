<?php
/**
 * @author Lauris Grosu celice95@gmail.com
 */
$config = include "config.php";
require_once "connection.php";
require_once "router.php";
require_once "model/model.php";
require_once "controller/AbstractController.php";

$page = new Router();
$page->start();
