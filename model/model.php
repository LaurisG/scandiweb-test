<?php
/**
 * @author Lauris Grosu celice95@gmail.com
 */
class Model
{
    private $db;

    /**
     * Model constructor.
     * @param $model
     */
    public function __construct($model)
    {
        $this->model = $model;
        $this->db = new Database();
    }

    /**
     * Getting all products from database
     * @return array|null
     */
    public function getAllProducts()
    {
        $result = $this->db->selectAll("*", "products_");
        $rows = null;
        foreach ($result as $row) {
            $rows[] = $row;
        }

        return $rows;
    }

    /**
     * Deleting products from database
     * @return string
     */
    public function deleteProducts()
    {
        if (isset($_POST) && !empty($_POST)) {
            try{
                foreach ($_POST["sku"] as $var) {
                   if (!$this->db->deleteProduct($var)){
                       throw new Exception();
                   }
                }
                return "OK";
            } catch ( Exception $e ){
                return "ERROR";
            }
        }
    }

    /**
     * Writing product to database
     * @return string
     */
    public function writeProduct()
    {
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $var) {
                if (empty($var)) {
                    return false;
                }
            }
            $insert_data = array(
                ':sku'      => $_POST['sku'],
                ':name'     => $_POST['name'],
                ':type'     => $_POST['type'],
                ':price'    => $_POST['price'],
                ':height'   => isset($_POST['attr']['height']) ? $_POST['attr']['height'] : null,
                ':width'    => isset($_POST['attr']['width']) ? $_POST['attr']['width'] : null,
                ':size'     => isset($_POST['attr']['size']) ? $_POST['attr']['size'] : null,
                ':lenght'   => isset($_POST['attr']['length']) ? $_POST['attr']['length'] : null,
                ':weight'   => isset($_POST['attr']['weight']) ? $_POST['attr']['weight'] : null
            );
            if($this->db->insert($insert_data)) {
                return "OK";
            } else {
                return "ERROR";
            }
        }
    }

    /**
     * Checking if sku exists
     * @return string
     */
    public function checkSKU()
    {
        if (isset($_POST) && !empty($_POST)) {
            return $this->db->getCountOfRows("sku", $_POST['sku']);
        }
    }
}
